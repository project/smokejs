<?php

/**
 * @file
 * Administration forms for the Smoke.js module.
 */

/**
 * Form constructor for Smoke.js settings form.
 *
 * @ingroup forms
 */
function smokejs_settings($form, &$form_state) {
  $library = libraries_detect('smoke.js');

  if ($library['installed']) {
    $form['smoke_js_library'] = array(
      '#type' => 'radios',
      '#title' => t('Smoke.js library'),
      '#default_value' => variable_get('smokejs_library', 'minified'),
      '#options' => array(
        'minified' => t('Minified'),
        'source' => t('Source/Uncompressed'),
      ),
    );

    return system_settings_form($form);
  }
  else {
    drupal_set_message($library['error message'], 'error');
  }
}
