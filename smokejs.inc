<?php

/**
 * @file
 * Functions to create smoke.js Ajax commands.
 *
 * @ingroup ajax_commands
 */

/**
 * Creates a smoke.js 'signal' command.
 *
 * @param string $text
 *   The message string to display to the user.
 *
 * @param array $settings
 *   An associative array containing:
 *   - duration: duration in miliseconds before the signal disappear.
 *   - classname: custom class for the signal
 *
 * @return array
 *   An array suitable for use with the ajax_render() function.
 */
function ajax_command_smoke_signal($text, $settings = array()) {
  $default = array(
    'duration' => 3000,
  );
  return array(
    'command' => 'smokeSignal',
    'text' => $text,
    'settings' => array_merge($default, $settings),
  );
}
